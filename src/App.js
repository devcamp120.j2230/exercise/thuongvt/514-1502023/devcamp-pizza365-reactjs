
import "bootstrap/dist/css/bootstrap.min.css"
import Content from "./component/Content/Content";
import Footer from "./component/Footer/footer";
import Header from "./component/Header/heade";
import ModalComponent from "./component/modal/modal";

function App() {
  return (
    <div>
      {/* header */}
      <Header />
      <div className="container" style={{ padding: "90px 0 50px 0;" }}>
        {/* <!-- Content --> */}
        <br></br>
        <Content />
        {/* <!-- 4. Footer --> */}
        <Footer />
        {/* Modal */}
        <ModalComponent/>
      </div>
    </div>
  );
}

export default App;
