import { Component } from "react";

class DrinkComponent extends Component {
    render() {
        return (
            <>
                {/* <!-- thanh tiêu dề chọn đồ uống --> */}
                <div className="row">
                    <div className="col-sm-12 text-center p-4 mt-4 " style={{ color: "#ff5722" }}>
                        <h2><b className="p-3 border-bottom">Chọn đồ uống</b></h2>
                    </div>
                    <select className="form-control" name="" id="select-drink">
                        <option value="">Tất cả các loại nước uống!</option>
                    </select>
                </div>
            </>
        )
    }
}