import { Component } from "react";

class SizeComponent extends Component {
    render() {
        return (
            <>
                {/* <!-- thanh tiêu dề --> */}
                <div className="row">
                    <div className="col-sm-12 text-center p-4 mt-4 " style={{ color: "#ff5722" }}>
                        <h2><b className="p-1 border-bottom">Menu Combo Pizza 365</b></h2>
                        <p><span className="p-2">“Hãy chọn cỡ pizza phù hợp với bạn!”</span></p>
                    </div>
                </div>

                {/* <!-- thanh nội dung --> */}
                <div className="col-sm-12">
                    <div className="row">
                        <div className="col-sm-4">
                            <div className="card" style={{ width: "18rem", height: "fit-content" }}>
                                <div className="card-header text-white text-center" style={{ backgroundColor: "#ff9800" }}>
                                    <h3>S(Small size)</h3>
                                </div>
                                <div className="card-body text-center">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Đường kính <b> 20 cm</b></li>
                                        <li className="list-group-item">Sườn nướng <b>2</b></li>
                                        <li className="list-group-item">Salad <b>200 gr</b></li>
                                        <li className="list-group-item">Nước ngọt <b>2</b></li>
                                        <li className="list-group-item">
                                            <h2>VND <b>150.000</b></h2>
                                        </li>
                                    </ul>
                                </div>
                                <div className="card-footer text-center">
                                    <button className="btn form-control" id="btn-small" style={{ background: "#ff9800" }}> Chọn</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <div className="card" style={{ width: "18rem", height: "fit-content" }}>
                                <div className="card-header text-white text-center" style={{ backgroundColor: "#d5e223" }}>
                                    <h3>M (Medium size)</h3>
                                </div>
                                <div className="card-body text-center">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Đường kính <b> 25 cm</b></li>
                                        <li className="list-group-item">Sườn nướng <b>4</b></li>
                                        <li className="list-group-item">Salad <b>300 gr</b></li>
                                        <li className="list-group-item">Nước ngọt <b>3</b></li>
                                        <li className="list-group-item">
                                            <h2>VND <b>200.000</b></h2>
                                        </li>
                                    </ul>
                                </div>
                                <div className="card-footer text-center">
                                    <button className="btn form-control" id="btn-medium" style={{ background: "#d5e223" }}> Chọn</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <div className="card" style={{ width: "18rem", height: "fit-content" }}>
                                <div className="card-header text-white text-center" style={{ backgroundColor: "#ff9800" }}>
                                    <h3>L (Large size )</h3>
                                </div>
                                <div className="card-body text-center">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Đường kính <b> 30 cm</b></li>
                                        <li className="list-group-item">Sườn nướng <b>8</b></li>
                                        <li className="list-group-item">Salad <b>500 gr</b></li>
                                        <li className="list-group-item">Nước ngọt <b>4</b></li>
                                        <li className="list-group-item">
                                            <h2>VND <b>250.000</b></h2>
                                        </li>
                                    </ul>
                                </div>
                                <div className="card-footer text-center">
                                    <button className="btn form-control" id="btn-large" style={{ background: "#ff9800" }}> Chọn</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default SizeComponent