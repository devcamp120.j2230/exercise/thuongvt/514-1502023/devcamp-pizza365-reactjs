import { Component } from "react";

class FromComponent extends Component {
    render() {
        return (
            <>
                {/* <!--Giủ đơn hàng --> */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="row">
                            <div className="col-sm-12 text-center p-4 mt-4 " style={{ color: "#ff5722" }}>
                                <h2><b className="p-3 border-bottom">Gửi đơn hàng</b></h2>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="form-group">
                                    <label for="">Tên</label>
                                    <input type="text" className="form-control inp-name" placeholder="Nhập tên" />
                                </div>
                                <div className="form-group">
                                    <label for="">Email</label>
                                    <input type="text" className="form-control inp-email" placeholder="Nhập Email" />
                                </div>
                                <div className="form-group">
                                    <label for="">Số điện thoại</label>
                                    <input type="text" className="form-control inp-phone" placeholder="Nhập Số điện thoại" />
                                </div>
                                <div className="form-group">
                                    <label for="">Địa chỉ</label>
                                    <input type="text" className="form-control inp-address" placeholder="Nhập Địa chỉ" />
                                </div>
                                <div className="form-group">
                                    <label for="">Mã giảm giá</label>
                                    <input type="text" className="form-control inp-voucher" placeholder="Nhập Mã giảm giá" />
                                </div>
                                <div className="form-group">
                                    <label for="">Lời nhắn</label>
                                    <input type="text" className="form-control inp-message" placeholder="Nhập Lời nhắn" />
                                </div>
                                <br></br>
                                <div className="form-group">
                                    <button type="button" className="form-control btn-send" style={{ backgroundColor: "#ff9800" }}>Gửi</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br></br>
            </>
        )
    }
}

export default FromComponent