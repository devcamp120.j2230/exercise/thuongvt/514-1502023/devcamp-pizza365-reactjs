
const { Component } = require("react");


class Header extends Component {
    render() {
        return (
            <div className="container-fluid bg-light">
                <div className="row">
                    <div className="col-sm-12">
                        <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-warning">
                            <div className="collapse navbar-collapse">
                                <ul className="navbar-nav nav-fill w-100">
                                    <li className="nav-item">
                                        <a className="nav-link" href={{}}>Trang chủ</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href={{}}>Combo</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href={{}}>Loại Pizza</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href={{}}>Giử đơn hàng</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        )
    }
}

export default Header