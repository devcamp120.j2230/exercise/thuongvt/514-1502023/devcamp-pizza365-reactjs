import { Component } from "react";

class Footer extends Component {
    render() {
        return (
            <div className="container bg-warning p-2">
                <div className="row text-center">
                    <div className="col-sm-12">
                        <h5 className="m-2">Footer</h5>
                        <a href={{}} className="btn btn-dark m-2"><i className="fa fa-arrow-up"></i>To the top</a>
                        <div className="m-2">
                            <i className="fa fa-facebook-official w3-hover-opacity"></i>
                            <i className="fa fa-instagram w3-hover-opacity"></i>
                            <i className="fa fa-snapchat w3-hover-opacity"></i>
                            <i className="fa fa-pinterest-p w3-hover-opacity"></i>
                            <i className="fa fa-twitter w3-hover-opacity"></i>
                            <i className="fa fa-linkedin w3-hover-opacity"></i>
                        </div>
                        <div className="p-1">
                            <p className="text-dark">Powered by DEVCAMP</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer